/*
 * Nikita S Morozov. Copyright (c) 2021.
 */

package amqp

import (
	"context"
	"errors"
	amqp "github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

func (c *Connection) consume(ctx context.Context, name string, consumer string) (<-chan amqp.Delivery, error) {
	c.Lock()
	data := c.queues[name]

	if data == nil {
		return nil, errors.New("no-queue-found")
	}

	ch, err := c.getOrCreateChannel(ctx, data)
	c.Unlock()

	if err != nil {
		return nil, err
	}

	return ch.Consume(name, consumer, true, false, false, false, nil)
}

func (c *Connection) handleConsumedDeliveries(ctx context.Context, name string, consumer string, delivery <-chan amqp.Delivery, fn func([]byte)) {
	for {
		select {
		case <-ctx.Done():
			return
		case record, ok := <-delivery:
			if !ok {
				c.logger.Error(
					"Delivery is closed",
					zap.String("name", name),
					zap.String("consumer", consumer),
				)
				return
			}

			fn(record.Body)
		}
	}
}
