/*
 * Nikita S Morozov. Copyright (c) 2021-2021.
 */

package amqp

import (
	"context"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/nikita.morozov/amqp-lib/v2/models"
	"go.uber.org/zap"
	"sync"
)

type Connection struct {
	sync.RWMutex

	logger   *zap.Logger
	amqpURI  string
	name     string
	conn     *amqp.Connection
	handler  *models.ConnectionHandler
	channels map[string]*amqp.Channel
	queues   map[string]*models.AmqpPublishData
	data     map[string]*models.AmqpPublishData
	debug    bool
	cancel   context.CancelFunc
}

// NewConnection returns the new connection object
func NewConnection(amqpURI string, name string, handler *models.ConnectionHandler, debug bool) *Connection {
	var logger *zap.Logger
	if debug {
		logger, _ = zap.NewDevelopment()
	} else {
		logger, _ = zap.NewProduction()
	}

	c := &Connection{
		logger:   logger,
		name:     name,
		amqpURI:  amqpURI,
		handler:  handler,
		channels: make(map[string]*amqp.Channel),
		queues:   make(map[string]*models.AmqpPublishData),
		data:     make(map[string]*models.AmqpPublishData),
	}

	return c
}

func (c *Connection) Connect(ctx context.Context, connectHandler *models.ConnectionHandler) {
	connectionCtx, cancel := context.WithCancel(ctx)
	c.cancel = cancel

	go func() {
		var err error

		for {
			c.conn, err = (*connectHandler)(c.amqpURI)

			c.logger.Info(
				"Connection",
				zap.String("url", c.amqpURI),
			)

			if err != nil {
				cancel()
				c.logger.Error(
					"Error in creating rabbitmq connection",
					zap.String("uri", c.amqpURI),
					zap.String("err", err.Error()),
				)
				return
			}

			// Update exchanges
			for _, data := range c.data {
				c.exchangeDeclare(ctx, data)
			}

			select {
			case <-connectionCtx.Done():
				for queue, data := range c.queues {
					c.Lock()
					chName := data.GetChannelName()
					ch, ok := c.channels[chName]

					if ok {
						ch.QueueUnbind(queue, data.Key, data.GetExchanger(), nil)
						ch.QueueDelete(queue, false, false, true)
						c.deleteQueue(queue)
						ch.ExchangeDelete(data.GetExchanger(), false, false)
					}

					delete(c.channels, chName)
					ch.Close()
					c.Unlock()
				}

				c.conn.Close()
				return
			case <-c.conn.NotifyClose(make(chan *amqp.Error)):
				continue
			}
		}
	}()
}

func (c *Connection) getOrCreateChannel(ctx context.Context, data *models.AmqpPublishData) (*amqp.Channel, error) {
	var err error
	c.logger.Info(
		"Get or Create channel",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic),
	)
	channelName := data.GetChannelName()

	c.logger.Info(
		"Get or Create channel 1",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic),
	)
	//c.Lock()

	c.logger.Info("Get or Create channel 2",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic))
	ch, ok := c.channels[channelName]
	c.logger.Info("Get or Create channel 3",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic))
	if !ok {
		c.logger.Info(
			"Channel not exist",
			zap.String("key", data.Key),
			zap.String("topic", data.Topic),
		)

		ch, err = c.conn.Channel()
		if err != nil {
			//c.Unlock()
			c.logger.Error(
				"Create channel",
				zap.String("key", data.Key),
				zap.String("topic", data.Topic),
				zap.String("channelName", channelName),
			)

			return nil, fmt.Errorf("Channel: %s", err)
		}

		c.channels[channelName] = ch

		go func() {
			channelName := channelName
			ch := c.channels[channelName]

			select {
			case <-ctx.Done():
				c.Lock()
				if ch, ok := c.channels[channelName]; ok {
					ch.Close()
				}
				c.Unlock()

			case <-ch.NotifyClose(make(chan *amqp.Error)):
				c.Lock()
				delete(c.channels, channelName)
				c.Unlock()
				return
			}
		}()
	}
	c.logger.Info("Get or Create channel 4",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic))
	//c.Unlock()
	c.logger.Info("Get or Create channel 5",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic))

	return ch, nil
}

func (c *Connection) queueBind(ctx context.Context, name string, data *models.AmqpPublishData) error {
	c.logger.Info(
		"Bind query",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic),
		zap.String("name", name),
	)

	c.Lock()
	ch, err := c.getOrCreateChannel(ctx, data)
	if err != nil {
		c.Unlock()

		c.logger.Error(
			"Bind query error",
			zap.String("key", data.Key),
			zap.String("topic", data.Topic),
			zap.String("name", name),
			zap.String("error", err.Error()),
		)
		return err
	}

	if _, err := ch.QueueDeclare(name, true, false, false, false, nil); err != nil {
		c.Unlock()

		c.logger.Error(
			"Bind query error",
			zap.String("key", data.Key),
			zap.String("topic", data.Topic),
			zap.String("name", name),
			zap.String("error", err.Error()),
		)
		return fmt.Errorf("error in declaring the queue %s", err)
	}

	if err := ch.QueueBind(name, data.Key, data.GetExchanger(), false, nil); err != nil {
		c.Unlock()
		c.logger.Error(
			"Bind query error",
			zap.String("key", data.Key),
			zap.String("topic", data.Topic),
			zap.String("name", name),
			zap.String("error", err.Error()),
			zap.String("exchanger", data.GetExchanger()),
		)

		return fmt.Errorf("Queue  Bind error: %s", err)
	}

	c.queues[name] = data
	c.Unlock()

	c.logger.Info(
		"Bind query done",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic),
		zap.String("name", name),
	)

	return nil
}

func (c *Connection) exchangeDeclare(ctx context.Context, data *models.AmqpPublishData) error {
	c.logger.Info(
		"Declare exchanger",
		zap.String("key", data.Key),
		zap.String("topic", data.Topic),
	)
	c.Lock()
	channel, err := c.getOrCreateChannel(ctx, data)
	if err != nil {
		c.Unlock()
		c.logger.Error(
			"Exchanger error",
			zap.String("key", data.Key),
			zap.String("topic", data.Topic),
			zap.String("error", err.Error()),
		)
		return err
	}

	if c.data[data.GetExchanger()] != nil {
		c.Unlock()

		c.logger.Info(
			"Exchanger",
			zap.String("key", data.Key),
			zap.String("topic", data.Topic),
		)
		return nil
	}

	err = channel.ExchangeDeclare(
		data.GetExchanger(), // name
		"topic",             // type
		true,                // durable
		true,                // auto-deleted
		false,               // internal
		false,               // noWait
		nil,                 // arguments
	)

	if err == nil {
		c.data[data.GetExchanger()] = data
	}

	c.Unlock()

	return err
}

func (c *Connection) deleteQueue(queue string) {
	delete(c.queues, queue)
}

func (c *Connection) Disconnect() {
	c.cancel()
}
