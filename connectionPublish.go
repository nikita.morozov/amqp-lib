/*
 * Nikita S Morozov. Copyright (c) 2021.
 */

package amqp

import (
	"context"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/nikita.morozov/amqp-lib/v2/models"
	"go.uber.org/zap"
)

func (c *Connection) Publish(body []byte, data models.AmqpPublishData) error {
	p := amqp.Publishing{
		Headers: amqp.Table{"type": "text/plain"},
		Body:    body,
	}

	c.Lock()

	ch, err := c.getOrCreateChannel(context.TODO(), &data)
	if err != nil {
		c.logger.Error(
			"Publish: get channel error",
			zap.String("key", data.Key),
			zap.String("exchanger", data.GetExchanger()),
			zap.String("body", string(body)),
		)
	}

	c.logger.Info(
		"Start publish",
		zap.String("key", data.Key),
		zap.String("exchanger", data.GetExchanger()),
		zap.String("body", string(body)),
	)

	err = ch.Publish(data.GetExchanger(), data.Key, false, false, p)
	c.Unlock()

	if err != nil {
		c.logger.Error(
			"Error in Publishing",
			zap.String("error", err.Error()),
		)

		return fmt.Errorf("Error in Publishing: %s", err)
	}

	c.logger.Info(
		"Publish",
		zap.String("key", data.Key),
		zap.String("exchanger", data.GetExchanger()),
		zap.String("body", string(body)),
	)

	return nil
}
