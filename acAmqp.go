/*
 * Nikita S Morozov. Copyright (c) 2021.
 */

package amqp

import (
	"context"
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/nikita.morozov/amqp-lib/v2/models"
	"go.uber.org/zap"
	"time"
)

type AcAmqp interface {
	Publish(ctx context.Context, data models.AmqpPublishData, payload interface{}) error
	Subscribe(ctx context.Context, data models.AmqpPublishData, queue string, handler func([]byte))
	CloseConnection()
}

type acAmqp struct {
	connection *Connection
}

func (a *acAmqp) Publish(ctx context.Context, data models.AmqpPublishData, payload interface{}) error {
	fmt.Printf("[*] AMQP publishing: %s, %s \n\n", data.Key, data.Topic)

	err := a.connection.exchangeDeclare(ctx, &data)
	if err != nil {
		return err
	}

	body, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	return a.connection.Publish(body, data)
}

func (a *acAmqp) Subscribe(ctx context.Context, data models.AmqpPublishData, queue string, handler func([]byte)) {
	fmt.Printf("[*] AMQP subscription: %s, %s \n\n", data.Key, data.Topic)

	var delivery <-chan amqp.Delivery

	go func() {
		fmt.Printf("[*] AMQP subscription: %s, %s; Retry!\n", data.Key, data.Topic)

		for {
			time.Sleep(1 * time.Second)

			err := a.connection.exchangeDeclare(ctx, &data)
			if err != nil {
				a.connection.logger.Error(
					"Subscribe: Declare exchanger",
					zap.String("key", data.Key),
					zap.String("topic", data.Topic),
					zap.String("error", err.Error()),
				)
				continue
			}

			err = a.connection.queueBind(ctx, queue, &data)
			if err != nil {
				a.connection.logger.Error(
					"Subscribe: Bind queue",
					zap.String("key", data.Key),
					zap.String("topic", data.Topic),
					zap.String("error", err.Error()),
				)
				continue
			}

			delivery, err = a.connection.consume(ctx, queue, a.connection.name)
			if err != nil {
				a.connection.logger.Error(
					"Subscribe: Consume error",
					zap.String("queue", queue),
					zap.String("connectionName", a.connection.name),
					zap.String("error", err.Error()),
				)

				a.connection.deleteQueue(queue)

				continue
			}

			a.connection.handleConsumedDeliveries(ctx, queue, a.connection.name, delivery, handler)

			// TODO: Stop it
		}
	}()
}

func (a *acAmqp) CloseConnection() {
	a.connection.Disconnect()
}

func NewAutoConnectAqmp(ctx context.Context, amqpURI string, name string, debug bool) AcAmqp {
	var handler models.ConnectionHandler = func(url string) (*amqp.Connection, error) {
		var c *amqp.Connection
		var err error

		for {
			c, err = amqp.Dial(url)

			if err != nil || c == nil {
				time.Sleep(1 * time.Second)
				continue
			}

			return c, err
		}
	}

	c := NewConnection(amqpURI, name, &handler, debug)
	c.Connect(ctx, &handler)

	return &acAmqp{
		connection: c,
	}
}
