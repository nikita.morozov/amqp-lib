/*
 * Nikita S Morozov. Copyright (c) 2021.
 */

package models

import (
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
)

// Message is the amqp request to publish
type Message struct {
	Data []byte
}

type AmqpPublishData struct {
	Topic string
	Key   string
}

func (pd *AmqpPublishData) GetExchanger() string {
	return fmt.Sprintf("%s.topic", pd.Topic)
}

func (pd *AmqpPublishData) GetChannelName() string {
	return fmt.Sprintf("channel.%s.%s", pd.Topic, pd.Key)
}

type ConnectionHandler = func(string) (*amqp.Connection, error)
